"""Main script for generating output.csv."""
import pandas as pd
class Solution(object):
    import pandas as pd
    def __init__(self):
        self.data = pd.read_csv('/Users/shijiezhou/Downloads/hiring_test/python_hiring_test/data/raw/pitchdata.csv')
        self.commands = list(open('/Users/shijiezhou/Downloads/hiring_test/python_hiring_test/data/reference/combinations.txt', 'r'))[1:]
        self.commands = [i.strip() for i in self.commands]
        self.result = []
        self.catch = []
    
    def generate(self):
        for command in self.commands:
            spec = command.split(',')
            stat, subject, split = spec[0], spec[1], spec[2]
            side = split[-3]
            pos = ''
            if split[-1] == 'H':
                pos = 'HitterSide'
            elif split[-1] == 'P':
                pos = 'PitcherSide'
            
            data2 = self.data[self.data[pos] == side]
            data3 = data2.groupby(subject).sum()
            data4 = data3[data3['PA'] >= 25]
            value = []
            if stat == 'AVG':
                value = data4['H'] / data4['AB']
            elif stat == 'OBP':
                value = (data4['H'] + data4['BB'] + data4['HBP']) / (data4['AB'] + data4['BB'] + data4['HBP'] + data4['SF'])
            elif stat == 'SLG':
                value = data4['TB']/data4['AB']
            elif stat == 'OPS':
                value = (data4['H'] + data4['BB'] + data4['HBP']) / (data4['AB'] + data4['BB'] + data4['HBP'] + data4['SF']) + data4['TB']/data4['AB']
            
            dic = {'SubjectId':value.index}
            result = pd.DataFrame(dic)
            result['Stat'] = [stat] * len(result)
            result['Split'] = [split] * len(result)
            result['Subject'] = [subject] * len(result)
            result['Value'] = value.tolist()
            self.result.append(result)
        re = pd.concat(self.result)
        re['Value'] = re['Value'].round(3)
        re = re.sort_values(['SubjectId','Stat','Split','Subject'])
        re.index = range(len(re))
        return re
def main():
    fir = Solution()
    re = fir.generate()
    # add basic program logic here
    re.to_csv('/Users/shijiezhou/Downloads/hiring_test/python_hiring_test/data/processed/output.csv',index=False)
    pass


if __name__ == '__main__':
    main()
